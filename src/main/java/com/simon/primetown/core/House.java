package com.simon.primetown.core;

import javax.persistence.*;
import java.util.Random;


@Entity
@Table(name = "house")
public class House{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "owner")
    private String owner;

    @Column(name = "yearofconstruction")
    private int yearofconstruction;

    @Column(name = "number")
    private int number;




    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public int getYearofconstruction() {

        Random rand = new Random();
        int randzahl;
        randzahl= 1800 + rand.nextInt(220);
        yearofconstruction = randzahl;
        return yearofconstruction;
    }

    public void setYearofconstruction(int yearOfConstruction) {
        this.yearofconstruction = yearOfConstruction;
    }

    public int getNumber() {

        number = getRandomPrimeNumber(Integer.MAX_VALUE);

        return number;

    }

    public static boolean isPrime( int n ) {
        if ( n < 2 ) {
            return false;
        }
        final int loopend = (int) Math.sqrt( n );
        for ( int i = 2; i <= loopend; i++ ) {
            if ( n % i == 0 ) {
                return false;
            }
        }
        return true;
    }

    public static int getRandomPrimeNumber( int pMax ) {
        final Random random = new Random();
        while ( true ) {
            final int nextInt = random.nextInt( pMax );
            if ( isPrime( nextInt ) ) {
                return nextInt;
            }
        }
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public House(){}


}